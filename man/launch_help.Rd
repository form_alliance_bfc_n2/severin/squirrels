% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/launch_help.R
\name{launch_help}
\alias{launch_help}
\title{Fonction permettant d'afficher les pages d'aide}
\usage{
launch_help()
}
\value{
1
}
\description{
Fonction affichant la page d'aide du package {squirrels}
}
\examples{
launch_help()
}
