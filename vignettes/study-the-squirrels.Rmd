---
title: "Study the squirrels"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{study-the-squirrels}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(squirrels)
```

<!-- WARNING - This vignette is generated by {fusen} from /dev/flat_study_squirrels.Rmd: do not edit by hand -->

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->


# get_message_fur_color

Objectif : Renvoyer à l'utilisateur la couleur de fourrure des écureuils de Central Park.
Paramètres : primary_fur_color un character correspondant à une couleur de fourrure des écureuils
Sortie : Un message renvoyant à l'utilisateur la couleur de fourrure de l'écureuil
    

  

```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Cinnamon")
```

  

  


# study_activity

    

  

```{r example-study_activity}
data(data_act_squirrels)
study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")

```

  

    

# Save an output as a csv file

You can save an output as a csv file with `save_as_csv()`. This function can be used, for instance, to save the filtered table created by `study_activity()`.
    

```{r example-save_as_csv}
# Create a temporary directory
my_temp_folder <- tempfile(pattern = "savecsv")
dir.create(my_temp_folder)

# Perform the analysis with study_activity()
res_activities_grat_squirrels <- study_activity(
  df_squirrels_act = data_act_squirrels, 
  col_primary_fur_color = "Gray"
  )

# Save res_activities_grat_squirrels$table as a csv
save_as_csv(res_activities_grat_squirrels$table, 
            path = file.path(my_temp_folder, "my_filtered_activities.csv"))

# See if the csv exists
list.files(my_temp_folder)

# Read the csv
read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))

# Delete the temporary folder
unlink(my_temp_folder, recursive = TRUE)
```

  

  

# read_data_day_squirrels

    

  

```{r example-read_data_day_squirrels}
library(purrr)
Tableau <- read_data_day_squirrels(system.file(package = "squirrels"))
Tableau

Tableau %>% 
  map(nrow)

Tableau %>% 
  compact()
```

  

  


