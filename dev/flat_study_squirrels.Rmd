---
title: "flat_minimal.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
library(ggplot2)
library(glue)
library(dplyr)
library(magrittr)
library(tools)
library(utils)
library(readxl)
library(purrr)
library(stringr)

primary_fur_color <- "Grey"

get_message_fur_color <- function(primary_fur_color){
message(glue("We will focus on {primary_fur_color} squirrels"))

}


possi_excel <- possibly(read_excel, otherwise = NULL)

nyc_squirrels_17 <- possi_excel(path = system.file("nyc_squirrels_17.xlsx", package = "squirrels"))
nyc_squirrels_18 <- possi_excel(path = system.file("nyc_squirrels_17.xlsx", package = "squirrels"))

Liste_excel <- list.files(path = system.file(package = "squirrels"), pattern = "nyc_squirrels_[0-9][0-9].xlsx", full.names = TRUE)
```

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# get_message_fur_color

Objectif : Renvoyer à l'utilisateur la couleur de fourrure des écureuils de Central Park.
Paramètres : primary_fur_color un character correspondant à une couleur de fourrure des écureuils
Sortie : Un message renvoyant à l'utilisateur la couleur de fourrure de l'écureuil
    
```{r function-get_message_fur_color}
#' Couleur de fourrure des écureuils
#' 
#' Fonction renvoyant à l'utilisateur un message spécifiant la couleur de l'écureuil
#' 
#' @param primary_fur_color character correspondant à la couleur de fourrure de l'écureuil
#' @importFrom glue glue
#' @return un message renvoyant la couleur de fourrure de l'écureuil à l'utilisateur
#' 
#' @export
get_message_fur_color <- function(primary_fur_color){
message(glue("We will focus on {primary_fur_color} squirrels"))

}
```
  
```{r example-get_message_fur_color}
get_message_fur_color(primary_fur_color = "Cinnamon")
```
  
```{r tests-get_message_fur_color}
test_that("get_message_fur_color works", {
  
  expect_message(
    object = get_message_fur_color(primary_fur_color = "Cinnamon"),
    regexp = "We will focus on Cinnamon squirrels") 
  
  expect_true(inherits(get_message_fur_color, "function"))
  
    expect_message(
    object = get_message_fur_color(primary_fur_color = "Black"),
    regexp = "We will focus on Black squirrels") 
  
    expect_message(
    object = get_message_fur_color(primary_fur_color = "grey"),
    regexp = "We will focus on grey squirrels") 
    
    expect_message(
    object = get_message_fur_color(primary_fur_color = 5),
    regexp = "We will focus on 5 squirrels") 
  
})


```
  

# study_activity
    
```{r function-study_activity}
#' Study the activities of the squirrels given a primary fur color
#'
#' @param df_squirrels_act Data frame. A dataset with the activities of the squirrels. This dataset mush have at leat these 4 columns: "age", "primary_fur_color", "activity", "counts".
#' @param col_primary_fur_color Character. The color of the primary fur color of interest. Only the squirrels with this primary fur color will be considered in the analysis.
#' 
#' @importFrom dplyr filter
#' @importFrom ggplot2 ggplot aes geom_col scale_fill_manual labs
#' @importFrom magrittr %>% 
#'
#' @return A list of two named elements. The first one is the filtered table. The second one is the ggplot.
#' @export
#'
#' @examples
study_activity <- function(df_squirrels_act, col_primary_fur_color) {
  
  if(isFALSE(is.data.frame(df_squirrels_act))) {
    stop("df_squirrels_act is not a data frame")
  }
  
  if(isFALSE(is.character(col_primary_fur_color))) {
    stop("col_primary_fur_color is not a character vector")
  }
  
  check_squirrel_data_integrity(df_squirrels_act)
  
  table <- df_squirrels_act %>%
    filter(primary_fur_color == col_primary_fur_color)
   
   graph <- table %>% 
     ggplot() +
     aes(x = activity, y = counts, fill = age) +
     geom_col() +
    labs(x = "Type of activity",
         y = "Number of observations",
         title = glue("Type of activity by age for {tolower(col_primary_fur_color)} squirrels")) +
    scale_fill_manual(name = "Age",
                      values = c("#00688B", "#00BFFF"))

  return(list(table = table, graph = graph))
}
```
  
```{r example-study_activity}
data(data_act_squirrels)
study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")

```
  
```{r tests-study_activity}
test_that("study_activity works", {
  expect_true(inherits(study_activity, "function")) 
})



test_that("valid_outputs", {
  
  data(data_act_squirrels)

  #Ok
results <- study_activity(df_squirrels_act = data_act_squirrels, 
               col_primary_fur_color = "Gray")

  expect_true(all(names(results) == c("table", "graph")))
  expect_true(inherits(results[[1]], "data.frame"))
  expect_true(inherits(results[[2]], "ggplot"))
  
  #Not ok
  expect_error(study_activity(df_squirrels_act = 3, 
                col_primary_fur_color = "Gray"), regexp = "df_squirrels_act is not a data frame")
  
  expect_error(study_activity(df_squirrels_act = data_act_squirrels, 
                col_primary_fur_color =3), regexp = "col_primary_fur_color is not a character vector")

})
```
    
# Save an output as a csv file

You can save an output as a csv file with `save_as_csv()`. This function can be used, for instance, to save the filtered table created by `study_activity()`.
    
```{r function-save_as_csv}
#' Save an output as a csv file
#'
#' @param data Data frame. The table to be saved as a csv file.
#' @param path Character. Path where the csv file shloud be saved.
#'
#' @importFrom tools file_ext
#' @importFrom utils write.csv2
#' 
#' @return The path to the created csv file.
#' @export
#'
#' @examples
save_as_csv <- function(data, path){
  
  
  if(isFALSE(is.data.frame(data))) {
    stop("data is not a data frame")
  }
  
  if(isFALSE(file_ext(path) == "csv")) {
    stop("path does not have extension csv")
  }  
  
  if(isTRUE(nrow(data) == 0)) {
    stop("data has an empty dimension")
  }  
  
  if(isFALSE(dir.exists(dirname(path)))) {
    stop("path does not exist")
  }    
  
  
  write.csv2(x = data, file = path, row.names = FALSE)
  
  return(invisible(path))
}
```

```{r example-save_as_csv}
# Create a temporary directory
my_temp_folder <- tempfile(pattern = "savecsv")
dir.create(my_temp_folder)

# Perform the analysis with study_activity()
res_activities_grat_squirrels <- study_activity(
  df_squirrels_act = data_act_squirrels, 
  col_primary_fur_color = "Gray"
  )

# Save res_activities_grat_squirrels$table as a csv
save_as_csv(res_activities_grat_squirrels$table, 
            path = file.path(my_temp_folder, "my_filtered_activities.csv"))

# See if the csv exists
list.files(my_temp_folder)

# Read the csv
read.csv2(file.path(my_temp_folder, "my_filtered_activities.csv"))

# Delete the temporary folder
unlink(my_temp_folder, recursive = TRUE)
```
  
```{r tests-save_as_csv}
test_that("save_as_csv works", {
  
  # Everything is ok
  my_temp_folder <- tempfile(pattern = "savecsv")
  dir.create(my_temp_folder)

  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")), 
              regexp = NA)
  
  expect_error(iris %>% save_as_csv(file.path(my_temp_folder, "output.csv")) %>% browseURL(), 
               regexp = NA)
  
  unlink(my_temp_folder, recursive = TRUE)
  
  # Error
  expect_error(iris %>% save_as_csv("output.xlsx"), 
               regexp = "does not have extension csv")
  
  expect_error(NULL %>% save_as_csv("output.csv"),
               regexp = "data is not a data frame")
  
  expect_error(iris %>% save_as_csv("does/no/exist.csv"), 
               regexp = "does not exist")
  
})
```
  
# read_data_day_squirrels
    
```{r function-read_data_day_squirrels}
#' Fonction permettant d'importer les fichiers de données d'écureuils
#' 
#' Description
#' 
#' @param chemin character le chemin d'accès au dossier qui contient les données
#' @return list de dataframe
#' @importFrom readxl read_excel
#' @importFrom stringr str_extract
#' @importFrom purrr is_empty
#' @importFrom purrr possibly
#' @importFrom purrr map
#' 
#' @export
read_data_day_squirrels <- function(chemin){
  
  if(isFALSE(file.exists(chemin))){
    stop("Le chemin d'acces precise n'existe pas")
  }
  
  Liste_excel <- list.files(path = chemin, pattern = "nyc_squirrels_[0-9][0-9].xlsx", full.names = TRUE)
  
    if(isTRUE(is_empty(Liste_excel))){
    stop("Le dossier ne contient pas de fichier sur les ecureuils")
  }
  
  possi_excel <- possibly(read_excel, otherwise = NULL)
  
  Jour_excel <-Liste_excel %>% 
    map(str_extract, pattern = "[0-9][0-9]")
  
    Matrice <- map(.x = Liste_excel,
         .f = possi_excel)
    
    names(Matrice) <- Jour_excel
    
    return(Matrice)
}
```
  
```{r example-read_data_day_squirrels}
library(purrr)
Tableau <- read_data_day_squirrels(system.file(package = "squirrels"))
Tableau

Tableau %>% 
  map(nrow)

Tableau %>% 
  compact()
```
  
```{r tests-read_data_day_squirrels}
test_that("read_data_day_squirrels works", {
  expect_true(inherits(read_data_day_squirrels, "function")) 
  
  expect_error(read_data_day_squirrels(chemin = "work"), "Le chemin d'acces precise n'existe pas")
  
  expect_error(read_data_day_squirrels(chemin = system.file(package = "dplyr")), "Le dossier ne contient pas de fichier sur les ecureuils")
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_study_squirrels.Rmd", vignette_name = "Study the squirrels")
```
