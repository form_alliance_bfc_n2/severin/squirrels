# squirrels 0.0.0.9000

 - Add `check_primary_color_is_ok()` to check if the color vector is OK

 - Add `get_message_fur_color()` to have a message with the fur color of squirrels
